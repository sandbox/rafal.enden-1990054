Description
This module adds possibility to filter taxonomy terms by term language.

Requirements
1. Locale module (in Drupal core).
2. Views (2.x) module (http://drupal.org/project/views).

Installation
1. Unpack and move directory "views_term_language" to your modules directory.
2. Enable it in the modules list of your site.
3. Create new view via "Views" module and choose "Term" type.
4. Add and configure filter "Taxonomy: Term Language" in "Taxonomy" group.
