<?php

/**
 * Implements hook_views_handlers().
 */
function views_term_language_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_term_language').'/includes',
    ),
    'handlers' => array(
      'views_handler_filter_term_language' => array(
        'parent' => 'views_handler_filter_locale_language',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function views_term_language_views_data() {
  $data = array();

  $data['term_data']['language'] = array(
    'title' => t('Term Language'),
    'help' => t('The taxonomy term language'),
    'filter' => array(
      'handler' => 'views_handler_filter_term_language',
      'hierarchy table' => 'term_hierarchy',
      'numeric' => FALSE,
      'skip base' => array('node', 'node_revision'),
    ),
  );
  
  return $data;
}
